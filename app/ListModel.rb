class ListModel

  attr_accessor :data

  # singleton method
  def self.shared
    Dispatch.once { @instance ||= new }
    @instance
  end

  def initialize
    load
  end

  def save
    file_location = NSURL.fileURLWithPath(File.join(NSHomeDirectory(), 'Documents', 'List.data')).path
    @data.writeToFile(file_location, atomically:true)
  end

  def load
    file_location = NSURL.fileURLWithPath(File.join(NSHomeDirectory(), 'Documents', 'List.data')).path
    puts file_location.class
    @data = []
    if File.exists?(file_location)
      @data = NSMutableArray.arrayWithContentsOfFile(file_location)
    else
      initData
    end
  end

  def initData
    @data = [
      { :name => "Todo 1" },
      { 
        :name => "Todo 2", 
        :children => [
          { :name => "Todo 2.1" }        ]
      },
      { 
        :name => "Todo 3",
        :children => [
          { :name => "Todo 3.1" },
          { 
            :name => "Todo 3.2",
            :children => [
              { :name => "3.2.1 Jaymini hello this is a very long sentence" },
              { :name => "3.2.3 Rob" }
            ]
          },
          { :name => "Todo 3.3" },
          { 
            :name => "Todo 3.4 with a really long item name that I hope will overflow - and even more content to make it really long",
            :children => [
              { :name => "3.4.1 Another Task" },
              { :name => "3.4.2 And another" },
              { :name => "3.4.3 And some more" }
            ]
          },
          { :name => "Todo 3.4" },
        ]
      },
      { :name => "Todo 4" },
      { :name => "Todo 5" },
      { :name => "Todo 6" },
      { :name => "Todo 7" },
      { :name => "Todo 8" },
      { :name => "Todo 9" },
      { :name => "Todo 10" },
      { :name => "Todo 11" },
      { :name => "Todo 12" }, 
      { :name => "" }
    ]
    @data
  end
end