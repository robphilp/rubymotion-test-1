class Object
  def with(*args, &block)
    if block_given?
      yield self, *args
    end
  end
end