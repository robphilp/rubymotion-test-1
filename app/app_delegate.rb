class AppDelegate
  def application(application, didFinishLaunchingWithOptions:launchOptions)
    @window = UIWindow.alloc.initWithFrame(UIScreen.mainScreen.bounds)
    controller = AppController.alloc.init
    controller.setData("Home", ListModel.shared.data)
    # puts(controller)
    @window.rootViewController = UINavigationController.alloc.initWithRootViewController(controller)
    @window.rootViewController.wantsFullScreenLayout = true
    @window.makeKeyAndVisible
    true
  end
end
