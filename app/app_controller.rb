class AppController < UITableViewController

  # inject title and data
  def setData(title, data)
    @title = title
    @data = data
    puts "@data = #{@data}"
  end

  def viewDidLoad
    super
    # view.dataSource = view.delegate = self # handled by superclass
    navigationItem.title = @title
    navigationItem.rightBarButtonItem = editButtonItem
    view.rowHeight = UITableViewAutomaticDimension;
    view.estimatedRowHeight = UITableViewAutomaticDimension
  end

  def viewDidAppear(animated)
    super(animated)
    view.reloadData
  end

  # Show "add" button on top-left when in edit mode,
  # else revert to standard "back" button
  def setEditing(isEditing, animated:isAnimated)
    if(isEditing)
      addButton = UIBarButtonItem.alloc.initWithBarButtonSystemItem(UIBarButtonSystemItemAdd, target:self, action:'addClick:')
      self.navigationItem.leftBarButtonItem = addButton
      view.setEditing(isEditing, animated:isAnimated)
    else
      self.navigationItem.leftBarButtonItem = nil
    end
    super
  end

  # Return size of current list
  def tableView(tableView, numberOfRowsInSection:section)
    @data.size
  end

  # Handle reordering of cells
  def tableView(tableView, moveRowAtIndexPath:fromIndexPath, toIndexPath:toIndexPath)
    item = @data.slice!(fromIndexPath.row)
    @data = @data.insert(toIndexPath.row, item)
  end

  def tableView(tableView, estimatedHeightForRowAtIndexPath:indexPath)
    UITableViewAutomaticDimension
  end

  # def backgroundColor(indexPath)
  #   UIColor.alloc.initWithRed(0.0, green:0.0, blue:(1.0 - 0.12 * indexPath.row / @data.count), alpha:1.0)
  # end

  # Return each list item
  CELLID = "ListItem"
  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cell = tableView.dequeueReusableCellWithIdentifier(CELLID) ||  begin
      cell = ListTableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:CELLID)
      cell.autoresizingMask = UIViewAutoresizingFlexibleHeight;
      cell.clipsToBounds = true;
      cell
    end
    # cell.backgroundColor = backgroundColor(indexPath)
    cell.data = @data[indexPath.row]
    # puts cell_data
    # cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping
    # cell.textLabel.text = cell.data[:name]
    # cell.textLabel.numberOfLines = 0
    # cell.accessoryView = cellActions(cell)
    cell.textLabel.tap do |label|
      label.lineBreakMode = NSLineBreakByWordWrapping
      label.text = cell.data[:name]
      label.numberOfLines = 0
    end
    cell.accessoryView = cellActions(cell)
    # cell.accessoryView = button
    cell
  end

  # def tableView(tableView, didSelectRowAtIndexPath:indexPath)
  #   @selectedRow = indexPath.row
  # end

  # Return edit type for cells
  def tableView(tableView, editingStyleForRowAtIndexPath:indexPath)
    UITableViewCellEditingStyleDelete
  end

  # Handle deletion of cells (delete button press)
  def tableView(tableView, commitEditingStyle:editingStyle, forRowAtIndexPath:indexPath)
    view.beginUpdates
    @data.slice!(indexPath.row)
    ListModel.shared.save
    view.deleteRowsAtIndexPaths([indexPath], withRowAnimation:UITableViewRowAnimationFade)
    view.endUpdates
  end

  # Handle drill-down into hierarchy by pushing table controller
  # with child items as data
  def drillDownClick(sender)
    # sender.superview.backgroundColor = UIColor.colorWithRed(1, green:0, blue:0, alpha:1)
    index = view.indexPathForCell(sender.superview.superview).row
    if(@data[index].has_key?(:children))
      @nextlevel_controller = AppController.alloc.init
      # puts @nextlevel_controller
      @nextlevel_controller.setData(@data[index][:name], @data[index][:children])
      self.navigationController.pushViewController(@nextlevel_controller, animated:true)
    end
  end

  # Create sub-view of buttons for each cell (assigned to accessoryView)
  def cellActions(cell)
    item_button_title = if(cell.data.has_key?(:children))
      "#{cell.data[:children].count} " + (cell.data[:children].count == 1 ? "item" : "items") 
    else 
      "0 items"
    end
    # item_button_title = (cell.data.has_key?(:children) ? "(#{cell.data[:children].count})" : "(0)")
    itemButton = UIButton.buttonWithType(UIButtonTypeRoundedRect)
    itemButton.frame = CGRectMake(0, 0, 80, 20)
    itemButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight
    itemButton.setTitle(item_button_title, forState:UIControlStateNormal)
    itemButton.addTarget(self, action:'drillDownClick:', forControlEvents:UIControlEventTouchUpInside)

    # addButton = UIButton.buttonWithType(UIButtonTypeRoundedRect)
    # addButton.frame = CGRectMake(80, 0, 20, 20)
    # addButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight
    # addButton.setTitle("+", forState:UIControlStateNormal)
    # addButton.addTarget(self, action:'addClick:', forControlEvents:UIControlEventTouchUpInside)

    buttonView = UIView.alloc.init
    buttonView.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 80, 20)
    # buttonView.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight
    buttonView.addSubview(itemButton)
    # buttonView.addSubview(addButton)
    buttonView
  end

end